package com.uwfx.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.uwfx.R;
import com.uwfx.adapter.IdentityResidentialProfileListAdapter;
import com.uwfx.databinding.FragmentChangePasswordBinding;
import com.uwfx.databinding.FragmentMyProfileBinding;
import com.uwfx.ui.activity.DashboardActivity;

import java.util.ArrayList;


public class ChangePasswordFragment extends Fragment {

    //UI declaration.


    //class object declaration..
    FragmentChangePasswordBinding mBinding;
    View rootView;

    Context mContext;
    //variable declaration.
    boolean isValidation = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_change_password, container, false);
        rootView = mBinding.getRoot();
        mContext=getActivity();
        prepareView();
        setOnClickListener();

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();

        ((DashboardActivity)mContext).visibleMenuBack("child");
        ((DashboardActivity)mContext).setHeaderTitle("Change Password");
        ((DashboardActivity)mContext).gonRightTitle();
        ((DashboardActivity)mContext).goneRightImage();
    }

    private void prepareView() {
        mBinding.tlCurrentPwd.setHint("Current Password");
        mBinding.tlNewPwd.setHint("New Password");
        mBinding.tlConfirmNewPwd.setHint("Confirm New Password");
    }

    private void setOnClickListener() {
        mBinding.tvPasswordType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.spPasswordList.performClick();
            }
        });

        final ArrayList<String> passwordTypeList = new ArrayList<>();

        passwordTypeList.add("Password Type");
        passwordTypeList.add("Cabinet");
        passwordTypeList.add("Withdrawal Pin");

        ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, passwordTypeList);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBinding.spPasswordList.setAdapter(aa);

        mBinding.spPasswordList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mBinding.tvPasswordType.setText(passwordTypeList.get(position).toString());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        mBinding.btnUpdatePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isValidation = true;


                if (mBinding.spPasswordList.getSelectedItemPosition() == 0) {
                    isValidation = false;
                    mBinding.tvPasswordType.setError(getString(R.string.Please_select_password_type));

                } else {
                    mBinding.tvPasswordType.setError(null);
                }

                if (TextUtils.isEmpty(mBinding.edtCurrentPwd.getText().toString())) {
                    isValidation = false;
                    mBinding.edtCurrentPwd.setError(getString(R.string.Please_provide_current_password));
                }
                if (TextUtils.isEmpty(mBinding.edtNewPwd.getText().toString())) {
                    isValidation = false;
                    mBinding.edtNewPwd.setError(getString(R.string.Please_provide_new_password));
                }

                if (mBinding.edtNewPwd.getText().toString().length() >= 1 && mBinding.edtNewPwd.getText().toString().length() < 6) {
                    isValidation = false;
                    mBinding.edtNewPwd.setError(getString(R.string.New_password_length_within_6_to_15_characters));
                }

                if (TextUtils.isEmpty(mBinding.edtConfirmNewPwd.getText().toString())) {
                    isValidation = false;
                    mBinding.edtConfirmNewPwd.setError(getString(R.string.Please_provide_confirm_new_password));
                }
                if (mBinding.edtConfirmNewPwd.getText().toString().length() >= 1 && !mBinding.edtConfirmNewPwd.getText().toString().equals(mBinding.edtNewPwd.getText().toString())) {
                    isValidation = false;
                    mBinding.edtConfirmNewPwd.setError(getString(R.string.New_password_not_matched));
                }

                if (isValidation) {
                    Log.e("yyy", "Validation");
                    Toast.makeText(getActivity(), "Successsfully Change Password", Toast.LENGTH_SHORT).show();
                    getActivity().getSupportFragmentManager().popBackStack();
                }

            }
        });
    }

}
