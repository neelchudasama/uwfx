package com.uwfx.fragment;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.uwfx.R;
import com.uwfx.databinding.FragmentAccountDetailsInfoMainBinding;
import com.uwfx.databinding.FragmentCreateNewTradeAccountBinding;
import com.uwfx.ui.activity.DashboardActivity;
import com.uwfx.ui.activity.SignaupStep2Activity;
import com.uwfx.ui.activity.SignaupStep3FinalActivity;

import java.util.ArrayList;


public class CreateNewTradeAccountFragment extends Fragment {

    //UI declaration.


    //class object declaration..
    FragmentCreateNewTradeAccountBinding mBinding;
    View rootView;
    Context mContext;

    //variable declaration.
    boolean isValidation = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_create_new_trade_account, container, false);
        rootView = mBinding.getRoot();
        mContext=getActivity();

        setOnClickListener();
        setAccountLeverageAndBonusList();

        return rootView;
    }




    @Override
    public void onResume() {
        super.onResume();

        ((DashboardActivity)mContext).visibleMenuBack("child");
        ((DashboardActivity)mContext).setHeaderTitle("Open A New Account");
        ((DashboardActivity)mContext).gonRightTitle();
        ((DashboardActivity)mContext).goneRightImage();
    }

    private void setOnClickListener() {
        mBinding.txtCreateTradeAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isValidation = true;

                if (mBinding.spAccountTypeList.getSelectedItemPosition() == 0) {
                    isValidation = false;
                    mBinding.tvAccountType.setError(getString(R.string.Please_select_account_type));
                } else {
                    mBinding.tvAccountType.setError(null);
                }
                if (mBinding.spLverageList.getSelectedItemPosition() == 0) {
                    isValidation = false;
                    mBinding.tvLeverage.setError(getString(R.string.Please_select_leverage));

                } else {
                    mBinding.tvLeverage.setError(null);
                }

                if (mBinding.spBonusOptionList.getSelectedItemPosition() == 0) {
                    isValidation = false;
                    mBinding.tvBonusOption.setError(getString(R.string.Please_select_bonus));

                } else {
                    mBinding.tvBonusOption.setError(null);
                }

                if(mBinding.cbTermsAndCondition.isChecked()==false){
                    isValidation = false;
                    Toast.makeText(getActivity(), "Please select terms and condition", Toast.LENGTH_SHORT).show();
                }
                else{

                }

                if(mBinding.cbPrivacyPolicy.isChecked()==false){
                    isValidation = false;
                    Toast.makeText(getActivity(), "Please select privacy policy", Toast.LENGTH_SHORT).show();
                }
                else{

                }


                if (isValidation) {
                    Log.e("yyy", "Validation");
                    Toast.makeText(mContext, "Successfully Created Trade Account", Toast.LENGTH_SHORT).show();
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });
    }

    private void setAccountLeverageAndBonusList() {

        mBinding.tvAccountType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBinding.spAccountTypeList.performClick();
            }
        });

        mBinding.tvLeverage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.spLverageList.performClick();

            }
        });

        mBinding.tvBonusOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.spBonusOptionList.performClick();

            }
        });

        final ArrayList<String> countryList = new ArrayList<>();
        final ArrayList<String> nationalityList = new ArrayList<>();
        final ArrayList<String> bonusList = new ArrayList<>();

        countryList.add("Account Type");
        for (int i = 0; i < 2; i++) {
            countryList.add(""+ i);
        }

        ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, countryList);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBinding.spAccountTypeList.setAdapter(aa);

        nationalityList.add("Leverage");
        for (int i = 0; i < 3; i++) {
            nationalityList.add(""+i);
        }


        ArrayAdapter aa1 = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, nationalityList);
        aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBinding.spLverageList.setAdapter(aa1);


        bonusList.add("Bonus");
        for (int i = 0; i < 4; i++) {
            bonusList.add(""+ i);
        }

        ArrayAdapter aa2 = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, bonusList);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBinding.spBonusOptionList.setAdapter(aa2);


        mBinding.spAccountTypeList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mBinding.tvAccountType.setText(countryList.get(position).toString());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mBinding.spLverageList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mBinding.tvLeverage.setText(nationalityList.get(position).toString());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mBinding.spBonusOptionList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mBinding.tvBonusOption.setText(bonusList.get(position).toString());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

}
