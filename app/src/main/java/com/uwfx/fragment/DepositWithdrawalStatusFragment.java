package com.uwfx.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uwfx.R;
import com.uwfx.adapter.DepositWithdrawalStatusListAdapter;
import com.uwfx.adapter.TransactionHistoryTypeListAdapter;
import com.uwfx.databinding.FragmentDepositwithdrawalStatusBinding;
import com.uwfx.databinding.FragmentTransactionhistoryDepositwithdrawalBinding;
import com.uwfx.ui.activity.DashboardActivity;


public class DepositWithdrawalStatusFragment extends Fragment {

    //UI declaration.


    //class object declaration..
    FragmentDepositwithdrawalStatusBinding mBinding;
    DepositWithdrawalStatusListAdapter depositWithdrawalStatusListAdapter;
    View rootView;
    Context mContext;
    //variable declaration.


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_depositwithdrawal_status, container, false);
        rootView = mBinding.getRoot();
        mContext = getActivity();
        depositWithdrawalStatusListAdapter = new DepositWithdrawalStatusListAdapter(getActivity());
        mBinding.lvDepositStatus.setAdapter(depositWithdrawalStatusListAdapter);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        ((DashboardActivity) mContext).visibleMenuBack("child");
        ((DashboardActivity) mContext).setHeaderTitle("Deposit Status");
        ((DashboardActivity) mContext).gonRightTitle();
        ((DashboardActivity) mContext).goneRightImage();
    }
}
