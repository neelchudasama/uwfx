package com.uwfx.fragment;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.uwfx.R;
import com.uwfx.databinding.FragmentAccountDetailsInfoMainBinding;
import com.uwfx.databinding.FragmentIbApplicationMainBinding;
import com.uwfx.ui.activity.DashboardActivity;
import com.uwfx.ui.activity.MainActivity;
import com.uwfx.ui.activity.VerificationCodeActivity;

import java.util.ArrayList;


public class IBApplicationMainFragment extends Fragment {

    //UI declaration.


    //class object declaration..
    FragmentIbApplicationMainBinding mBinding;
    View rootView;
    Context mContext;

    //variable declaration.
    boolean isValidation = true;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_ib_application_main, container, false);
        rootView = mBinding.getRoot();
        mContext=getActivity();

        setUp();

        setOnClickListener();
        return rootView;
    }



    private void setUp() {

        mBinding.txtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isValidation = true;

                if (mBinding.sptradeAccountList.getSelectedItemPosition() == 0) {
                    isValidation = false;
                    mBinding.tvTradeAccount.setError(getString(R.string.Please_select_trading_account_no));

                } else {
                    mBinding.tvTradeAccount.setError(null);
                }

               /* if (TextUtils.isEmpty(mBinding.edtMessage.getText().toString())) {
                    isValidation = false;
                    mBinding.edtMessage.setError(getString(R.string.Please_provide_verification_code));
                }*/

                if (mBinding.cbTermsAndCondition.isChecked() == false) {
                    isValidation = false;
                    Toast.makeText(mContext, "Please agree to partnership Terms and Conditions.", Toast.LENGTH_SHORT).show();
                } else {

                }




                if (isValidation) {
                    Log.e("yyy", "Validation");
                    Toast.makeText(mContext, "Your IB application submitted successfully.", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    private void setOnClickListener() {
        mBinding.tvTradeAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.sptradeAccountList.performClick();
            }
        });

        final ArrayList<String> passwordTypeList = new ArrayList<>();

        passwordTypeList.add("Trade Account");
        passwordTypeList.add("100012");
        passwordTypeList.add("100013");
        passwordTypeList.add("100014");

        ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, passwordTypeList);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBinding.sptradeAccountList.setAdapter(aa);

        mBinding.sptradeAccountList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mBinding.tvTradeAccount.setText(passwordTypeList.get(position).toString());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        ((DashboardActivity)mContext).visibleMenuBack("child");
        ((DashboardActivity)mContext).setHeaderTitle("IB Application");
        ((DashboardActivity)mContext).gonRightTitle();
        ((DashboardActivity)mContext).goneRightImage();
    }


}
