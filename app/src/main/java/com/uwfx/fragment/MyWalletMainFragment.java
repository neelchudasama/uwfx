package com.uwfx.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;

import com.uwfx.R;
import com.uwfx.adapter.MyAccountMainTradeListAdapter;
import com.uwfx.databinding.FragmentMyAccountMainBinding;
import com.uwfx.databinding.FragmentMyWalletMainBinding;
import com.uwfx.ui.activity.DashboardActivity;


public class MyWalletMainFragment extends Fragment {

    //UI declaration.


    //class object declaration..
    FragmentMyWalletMainBinding mBinding;
    View rootView;
    Context mContext;
    //variable declaration.
    TranslateAnimation imageAnimation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_my_wallet_main, container, false);
        rootView = mBinding.getRoot();
        mContext=getActivity();
        prepareView();
        setOnActionListener();

        return rootView;
    }
    @Override
    public void onResume() {
        super.onResume();
        ((DashboardActivity)mContext).setHeaderTitle("My Wallet");
        ((DashboardActivity)mContext).visibleMenuBack("main");
        ((DashboardActivity)mContext).visibleRighttitle("History");
        ((DashboardActivity)mContext).goneRightImage();
    }

    private void prepareView() {
        mBinding.lnBoldDeposit.setVisibility(View.VISIBLE);
        mBinding.lnSimpleWithdrawal.setVisibility(View.VISIBLE);

        DisplayMetrics outMetrics = new DisplayMetrics();

        getActivity().getWindowManager().getDefaultDisplay().getMetrics(outMetrics);

        imageAnimation = new TranslateAnimation(outMetrics.widthPixels,0 , 0, 0);
        imageAnimation.setDuration(700);

    }

    private void setOnActionListener() {
        int[] viewToBeMovedPos = new int[2];
        int[] destinationViewPos = new int[2];
        mBinding.viewDepositFundsOptions.getLocationOnScreen(viewToBeMovedPos);
        mBinding.viewWithdrawalFundsOptions.getLocationOnScreen(destinationViewPos);

        final int xDiff = destinationViewPos[0] - viewToBeMovedPos[0];
        final int yDiff = destinationViewPos[1] - viewToBeMovedPos[1];
        mBinding.lnSimpleDeposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


              //  mBinding.viewDepositFundsOptions.animate().translationXBy(xDiff).translationYBy(yDiff);
                DisplayMetrics outMetrics = new DisplayMetrics();

                getActivity().getWindowManager().getDefaultDisplay().getMetrics(outMetrics);

                TranslateAnimation imageAnimation = new TranslateAnimation(0 ,outMetrics.widthPixels, 0, 0);
                imageAnimation.setDuration(700);

                mBinding.viewWithdrawalFundsOptions.startAnimation(imageAnimation);

                mBinding.lnSimpleDeposit.setVisibility(View.GONE);
                mBinding.lnBoldDeposit.setVisibility(View.VISIBLE);
                mBinding.lnSimpleWithdrawal.setVisibility(View.VISIBLE);
                mBinding.btnDepositStatus.setText("Deposit Status");


            }
        });


        mBinding.lnSimpleWithdrawal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayMetrics outMetrics = new DisplayMetrics();

                getActivity().getWindowManager().getDefaultDisplay().getMetrics(outMetrics);

                TranslateAnimation imageAnimation = new TranslateAnimation(outMetrics.widthPixels ,0, 0, 0);
                imageAnimation.setDuration(700);

                mBinding.viewDepositFundsOptions.startAnimation(imageAnimation);

             //   mBinding.viewWithdrawalFundsOptions.animate().translationXBy(xDiff).translationYBy(yDiff);

                mBinding.lnSimpleWithdrawal.setVisibility(View.GONE);
                mBinding.lnBoldDeposit.setVisibility(View.GONE);
                mBinding.lnSimpleDeposit.setVisibility(View.VISIBLE);
                mBinding.lnBoldWithdrawal.setVisibility(View.VISIBLE);
                mBinding.btnDepositStatus.setText("Withdrawal Status");

            }
        });

        mBinding.btnDepositStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DepositWithdrawalStatusFragment fragment = new DepositWithdrawalStatusFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                            fm.popBackStack();

                        }
                        MyProfileFragment fragment = new MyProfileFragment();
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).commit();
                        return true;
                    }
                }
                return false;
            }
        });
    }

}
