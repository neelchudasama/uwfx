package com.uwfx.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.uwfx.R;
import com.uwfx.ui.activity.DashboardActivity;


/**
 * Created by Dharmesh on 2/9/2016.
 */

public class WebViewForAboutusFragment extends Fragment {

    View mRootView;
    WebView myWebView;

    ProgressDialog dialog;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_webview_menu, container, false);
        dialog = new ProgressDialog(getActivity());
        context=getActivity();
        myWebView = (WebView) mRootView.findViewById(R.id.webview);


        ((DashboardActivity)context).setHeaderTitle("WebView");
        ((DashboardActivity)context).visibleMenuBack("main");
        ((DashboardActivity)context).gonRightTitle();
        ((DashboardActivity)context).goneRightImage();

        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        dialog.setMessage("Loading..Please wait.");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

       // if (menu.equals("aboutus")) {
            myWebView.loadUrl("https://uwforex-cn.com/zh/");
     /*   } else if (menu.equals("terms")) {
            Pref.setValue(getActivity(), "current", "terms");
            myWebView.loadUrl("http://ww24connect.com/index.php/Page/termcondition");
        } else {
            Pref.setValue(getActivity(), "current", "privacy");
            myWebView.loadUrl("http://ww24connect.com/index.php/Page/privacy");
        }*/

        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        return mRootView;
    }
}
