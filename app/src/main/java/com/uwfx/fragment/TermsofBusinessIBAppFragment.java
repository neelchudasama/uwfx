package com.uwfx.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uwfx.R;
import com.uwfx.databinding.FragmentAccountDetailsInfoMainBinding;
import com.uwfx.databinding.FragmentTermsOfBusinessIbAppBinding;
import com.uwfx.ui.activity.DashboardActivity;


public class TermsofBusinessIBAppFragment extends Fragment {

    //UI declaration.


    //class object declaration..
    FragmentTermsOfBusinessIbAppBinding mBinding;
    View rootView;
    Context mContext;
    //variable declaration.


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_terms_of_business_ib_app, container, false);
        rootView = mBinding.getRoot();
        mContext=getActivity();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((DashboardActivity)mContext).visibleMenuBack("child");
        ((DashboardActivity)mContext).setHeaderTitle("Terms of Business");
        ((DashboardActivity)mContext).gonRightTitle();
        ((DashboardActivity)mContext).goneRightImage();
    }
}
