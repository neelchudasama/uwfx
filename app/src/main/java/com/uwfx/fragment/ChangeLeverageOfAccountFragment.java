package com.uwfx.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uwfx.R;
import com.uwfx.databinding.FragmentAccountDetailsInfoMainBinding;
import com.uwfx.databinding.FragmentChangeLeverageAccountBinding;
import com.uwfx.ui.activity.DashboardActivity;


public class ChangeLeverageOfAccountFragment extends Fragment {

    //UI declaration.


    //class object declaration..
    FragmentChangeLeverageAccountBinding mBinding;
    View rootView;
    Context mContext;
    //variable declaration.


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_change_leverage_account, container, false);
        rootView = mBinding.getRoot();
        mContext=getActivity();
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        ((DashboardActivity)mContext).visibleMenuBack("child");
        ((DashboardActivity)mContext).setHeaderTitle("Change Leverage");
        ((DashboardActivity)mContext).gonRightTitle();
        ((DashboardActivity)mContext).goneRightImage();
    }
}
