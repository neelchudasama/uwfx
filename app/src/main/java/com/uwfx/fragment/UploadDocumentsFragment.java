package com.uwfx.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.uwfx.R;
import com.uwfx.adapter.IdentityResidentialProfileListAdapter;
import com.uwfx.adapter.UploadIdentityResidentialAdapter;
import com.uwfx.databinding.FragmentMyProfileBinding;
import com.uwfx.databinding.FragmentUploadDocumentsBinding;
import com.uwfx.ui.activity.DashboardActivity;


public class UploadDocumentsFragment extends Fragment {

    //UI declaration.


    //class object declaration..
    FragmentUploadDocumentsBinding mBinding;
    View rootView;
    Context mContext;
    //variable declaration.
    int uploadType = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_upload_documents, container, false);
        rootView = mBinding.getRoot();
        mContext=getActivity();
        prepareView();
        setOnClickListener();

        UploadIdentityResidentialAdapter uploadIdentityResidentialAdapter = new UploadIdentityResidentialAdapter(getActivity());
        mBinding.lvIdentityResidentialUpload.setAdapter(uploadIdentityResidentialAdapter);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        ((DashboardActivity)mContext).visibleMenuBack("child");
        ((DashboardActivity)mContext).setHeaderTitle("Upload Documents");
        ((DashboardActivity)mContext).gonRightTitle();
        ((DashboardActivity)mContext).goneRightImage();

    }

    private void prepareView() {
        mBinding.tvIdentityType.setTextColor(getResources().getColor(R.color.white));
        mBinding.tvResidentialType.setTextColor(getResources().getColor(R.color.colorPrimary));

    }

    private void setOnClickListener() {
        mBinding.tvIdentityType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setBackgoundView(0);

            }
        });


        mBinding.tvResidentialType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBackgoundView(1);
            }
        });

        mBinding.btnUploadDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UploadDocumentOptionTakePhotoGalleryFragment fragment = new UploadDocumentOptionTakePhotoGalleryFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).addToBackStack(null).commit();
            }
        });
    }

    private void setBackgoundView(int i) {
        if (i == 0) {
            mBinding.tvIdentityType.setTextColor(getResources().getColor(R.color.white));
            mBinding.tvResidentialType.setTextColor(getResources().getColor(R.color.colorPrimary));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 1;
            params.setMargins(2, 2, 2, 2);
            mBinding.tvResidentialType.setLayoutParams(params);
            mBinding.tvIdentityType.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            mBinding.tvResidentialType.setBackgroundResource(R.drawable.round_background_white);

        } else {
            mBinding.tvIdentityType.setTextColor(getResources().getColor(R.color.colorPrimary));
            mBinding.tvResidentialType.setTextColor(getResources().getColor(R.color.white));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight = 1;
            params.setMargins(2, 2, 2, 2);
            mBinding.tvIdentityType.setLayoutParams(params);
            mBinding.tvResidentialType.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            mBinding.tvIdentityType.setBackgroundResource(R.drawable.round_background_white);

        }
    }

}
