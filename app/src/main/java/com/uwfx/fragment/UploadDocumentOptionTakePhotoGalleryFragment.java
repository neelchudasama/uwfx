package com.uwfx.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uwfx.R;
import com.uwfx.adapter.IdentityResidentialProfileListAdapter;
import com.uwfx.databinding.FragmentMyProfileBinding;
import com.uwfx.databinding.FragmentUploadDocumentOptionTakecameragalleryBinding;
import com.uwfx.ui.activity.DashboardActivity;


public class UploadDocumentOptionTakePhotoGalleryFragment extends Fragment {

    //UI declaration.


    //class object declaration..
    FragmentUploadDocumentOptionTakecameragalleryBinding mBinding;
    View rootView;
    Context mContext;
    //variable declaration.


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_upload_document_option_takecameragallery, container, false);
        rootView = mBinding.getRoot();
        mContext=getActivity();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((DashboardActivity)mContext).visibleMenuBack("child");
        ((DashboardActivity)mContext).setHeaderTitle("Upload Documents");
        ((DashboardActivity)mContext).gonRightTitle();
        ((DashboardActivity)mContext).goneRightImage();
    }
}
