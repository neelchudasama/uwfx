package com.uwfx.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uwfx.R;
import com.uwfx.adapter.IdentityResidentialProfileListAdapter;
import com.uwfx.databinding.FragmentMyProfileBinding;
import com.uwfx.databinding.FragmentParternershipBinding;
import com.uwfx.ui.activity.DashboardActivity;


public class ParternershipMainFragment extends Fragment {

    //UI declaration.


    //class object declaration..
    FragmentParternershipBinding mBinding;
    View rootView;
    Context mContext;
    //variable declaration.


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_parternership, container, false);
        rootView = mBinding.getRoot();
        mContext=getActivity();

        mBinding.lnMyNetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebViewForAboutusFragment webViewForAboutusFragment5 = new WebViewForAboutusFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, webViewForAboutusFragment5).addToBackStack(null).commit();
            }
        });

        mBinding.lnIbApplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IBApplicationMainFragment fragment = new IBApplicationMainFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).addToBackStack(null).commit();
            }
        });

        mBinding.lnTermsOfBusiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TermsofBusinessIBAppFragment fragment = new TermsofBusinessIBAppFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).addToBackStack(null).commit();
            }
        });
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        ((DashboardActivity)mContext).setHeaderTitle("Partnership");
        ((DashboardActivity)mContext).visibleMenuBack("main");
        ((DashboardActivity)mContext).gonRightTitle();
        ((DashboardActivity)mContext).goneRightImage();
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                            fm.popBackStack();

                        }
                        MyProfileFragment fragment = new MyProfileFragment();
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).commit();
                        return true;
                    }
                }
                return false;
            }
        });
    }
}
