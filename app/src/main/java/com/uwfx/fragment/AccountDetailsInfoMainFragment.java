package com.uwfx.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.uwfx.R;
import com.uwfx.databinding.FragmentAccountDetailsInfoMainBinding;
import com.uwfx.databinding.FragmentContactusBinding;
import com.uwfx.ui.activity.DashboardActivity;


public class AccountDetailsInfoMainFragment extends Fragment {

    //UI declaration.


    //class object declaration..
    FragmentAccountDetailsInfoMainBinding mBinding;
    View rootView;
    Context mContext;
    Dialog captchadialog;
    ImageView mCaptchaImg;
    ProgressBar mProgressbar;
    //variable declaration.


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_account_details_info_main, container, false);
        rootView = mBinding.getRoot();
        mContext=getActivity();

        mBinding.imgChangeLeverage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeLeverageOfAccountFragment fragment = new ChangeLeverageOfAccountFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).addToBackStack(null).commit();
            }
        });

        mBinding.tvResetTradePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warning("3004541");
            }
        });

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();

        ((DashboardActivity)mContext).visibleMenuBack("child");
        ((DashboardActivity)mContext).setHeaderTitle("Account Details");
        ((DashboardActivity)mContext).gonRightTitle();
        ((DashboardActivity)mContext).goneRightImage();
    }

    public void warning(final String header) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Reset Trading Password");
        alertDialogBuilder.setMessage(getActivity().getResources().getString(R.string.Are_you_sure_you_want_to_reset_password_for_trading_password) + " " + header + "?");
        // set positive button: Yes message
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {



                captchaCodeDialog(getActivity(), header);



            }
        });
        // set negative button: No message
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // cancel the alert box and put a Toast to the user
                dialog.cancel();

            }
        });
        // set neutral button: Exit the app message

        AlertDialog alertDialog = alertDialogBuilder.create();
        // show alert
        alertDialog.show();

        //Toast.makeText(MainActivity.this,"logout",Toast.LENGTH_LONG).show();

    }


    /**
     * OLD ONE
     * dialog for get the captcha code
     */
    private void captchaCodeDialog(final Context context, final String value) {

        captchadialog = new Dialog(context);
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        captchadialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        captchadialog.setContentView(R.layout.dialog_captcha_code);
        captchadialog.setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = captchadialog.getWindow();
        lp.copyFrom(window.getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        /*getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        lp.width = width - 100;
        lp.height = height - 20;*/
        window.setAttributes(lp);

        captchadialog.getWindow().setGravity(Gravity.CENTER);
        captchadialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));


        mCaptchaImg = (ImageView) captchadialog.findViewById(R.id.img_captchacode);
        final EditText mCaptchaCodeEdt = (EditText) captchadialog.findViewById(R.id.edt_captchacode);
        TextView mSendTv = (TextView) captchadialog.findViewById(R.id.tv_submit_qrcode);
        TextView mCloseTv=(TextView)captchadialog.findViewById(R.id.tv_close_qrcode);
        mProgressbar = (ProgressBar) captchadialog.findViewById(R.id.pbProgress);



        mCloseTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                captchadialog.dismiss();
            }
        });
        mSendTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mCaptchaCodeEdt.getText().toString())) {

                    //captchadialog.dismiss();
                } else {
                    // mValidator.showtoast("Please enter captcha code.");
                }

            }
        });
        captchadialog.show();
    }

}
