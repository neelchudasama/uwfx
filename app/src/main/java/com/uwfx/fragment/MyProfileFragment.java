package com.uwfx.fragment;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.transition.Fade;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.uwfx.R;
import com.uwfx.adapter.IdentityResidentialProfileListAdapter;
import com.uwfx.databinding.FragmentMyProfileBinding;
import com.uwfx.ui.activity.DashboardActivity;
import com.uwfx.utils.Sample;

import java.util.Arrays;
import java.util.List;


public class MyProfileFragment extends Fragment {

    //UI declaration.
    ListView lv_identity_residential_profile;
    ImageView img_camera_option;
    ImageView img_user_profile;
    FragmentMyProfileBinding binding;
    ObjectAnimator objectanimator;
    private ViewGroup viewRoot;
    private Sample sample;
    private List<Sample> samples;
    Context mContext;

    //class object declaration..
    View rootView;


    //variable declaration.
    private boolean positionChanged;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding= DataBindingUtil.inflate(
                inflater, R.layout.fragment_my_profile, container, false);
        rootView = binding.getRoot();
        mContext=getActivity();
        samples = Arrays.asList(
                new Sample(getResources().getColor(R.color.colorPrimary), "View animations")
        );
        binding.setAnimationsSample(samples.get(0));



        setupWindowAnimations();
        initComponent();
        prepareView();
        setOnClickListener();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((DashboardActivity)mContext).setHeaderTitle("My Profile");
        ((DashboardActivity)mContext).visibleMenuBack("main");
        ((DashboardActivity)mContext).visibleRightImage(R.mipmap.home);
        ((DashboardActivity)mContext).gonRightTitle();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setupWindowAnimations() {
        getActivity().getWindow().setReenterTransition(new Fade());
    }



    private void initComponent() {
        lv_identity_residential_profile=(ListView)rootView.findViewById(R.id.lv_identity_residential_profile);
        img_camera_option=(ImageView)rootView.findViewById(R.id.img_camera_option);
        img_user_profile=(ImageView)rootView.findViewById(R.id.img_user_profile);
        viewRoot = (ViewGroup)rootView.findViewById(R.id.ln_main_anim);

        objectanimator = ObjectAnimator.ofFloat(img_user_profile,"x",200);
    }

    private void prepareView() {
        IdentityResidentialProfileListAdapter identityResidentialProfileListAdapter=new IdentityResidentialProfileListAdapter(getActivity());
        lv_identity_residential_profile.setAdapter(identityResidentialProfileListAdapter);
    }

    private void setOnClickListener() {
        img_camera_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objectanimator.setDuration(1000);
                objectanimator.start();
                viewRoot.setVisibility(View.VISIBLE);
               //changePosition();
            }
        });

    }


    private void changePosition() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition(viewRoot);
        }

        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) img_user_profile.getLayoutParams();
        if (positionChanged) {
            //lp.addRule(Gravity.CENTER);
            lp.gravity = Gravity.CENTER;
        } else {
           // lp.addRule(Gravity.LEFT);
            lp.gravity = Gravity.LEFT;
        }
        positionChanged = !positionChanged;
        img_user_profile.setLayoutParams(lp);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

        getView().setOnKeyListener(new View.OnKeyListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                       // getActivity().getSupportFragmentManager().popBackStack();
                        ((DashboardActivity) getActivity()).finishAffinity();
                        return true;
                    }
                }
                return false;
            }
        });
    }

}
