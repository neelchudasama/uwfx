package com.uwfx.fragment;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.uwfx.R;
import com.uwfx.databinding.FragmentContactusBinding;
import com.uwfx.databinding.FragmentParternershipBinding;
import com.uwfx.ui.activity.DashboardActivity;


public class ContactUsMainFragment extends Fragment {

    //UI declaration.


    //class object declaration..
    FragmentContactusBinding mBinding;
    View rootView;
    Context mContext;
    //variable declaration.


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_contactus, container, false);
        rootView = mBinding.getRoot();
        mContext=getActivity();

        mBinding.tvEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.putExtra(android.content.Intent.EXTRA_EMAIL, mBinding.tvEmail.getText());
                i.putExtra(android.content.Intent.EXTRA_SUBJECT, "feedback");
                i.putExtra(android.content.Intent.EXTRA_TEXT, "welcom");
                startActivity(Intent.createChooser(i, "Send email"));
            }
        });

        return rootView;
    }



    @Override
    public void onResume() {
        super.onResume();
        ((DashboardActivity)mContext).setHeaderTitle("Contact Us");
        ((DashboardActivity)mContext).visibleMenuBack("main");
        ((DashboardActivity)mContext).gonRightTitle();
        ((DashboardActivity)mContext).goneRightImage();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                            fm.popBackStack();

                        }
                        MyProfileFragment fragment = new MyProfileFragment();
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).commit();
                        return true;
                    }
                }
                return false;
            }
        });
    }

}
