package com.uwfx.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.uwfx.R;
import com.uwfx.adapter.IdentityResidentialProfileListAdapter;
import com.uwfx.adapter.MyAccountMainTradeListAdapter;
import com.uwfx.databinding.FragmentAccountDetailsInfoMainBinding;
import com.uwfx.databinding.FragmentMyAccountMainBinding;
import com.uwfx.databinding.FragmentMyProfileBinding;
import com.uwfx.interfaces.OnClickOpenAccountLeverageListener;
import com.uwfx.ui.activity.DashboardActivity;


public class MyAccountMainFragment extends Fragment {

    //UI declaration.


    //class object declaration..
    FragmentMyAccountMainBinding mBinding;
    View rootView;
    Dialog listDialog;
    Dialog captchadialog;
    Context context;
    ImageView mCaptchaImg;
    ProgressBar mProgressbar;
    //variable declaration.


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_my_account_main, container, false);
        rootView = mBinding.getRoot();
        context=getActivity();

        MyAccountMainTradeListAdapter myAccountMainTradeListAdapter=new MyAccountMainTradeListAdapter(getActivity());
        mBinding.lvMyAccount.setAdapter(myAccountMainTradeListAdapter);
        myAccountMainTradeListAdapter.onClickUnLinkedCurrentTradeAccListener(onClickOpenAccountLeverageListener);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((DashboardActivity)context).setHeaderTitle("My Account");
        ((DashboardActivity)context).visibleMenuBack("main");
        ((DashboardActivity)context).visibleRighttitle("Add");
        ((DashboardActivity)context).goneRightImage();
    }

    OnClickOpenAccountLeverageListener onClickOpenAccountLeverageListener=new OnClickOpenAccountLeverageListener() {
        @Override
        public void OnClickUnLinkedCurrentTradeAccListener(int payloadLinkedDeviceListModel) {
            openDialogUnlinkTradeAccount(payloadLinkedDeviceListModel);
        }
    };


    /**
     * dialog for option of create unlinked with device funOs and smart watch
     */
    private void openDialogUnlinkTradeAccount(final int myTradingAccountDeviceManagerDataList) {

        listDialog = new Dialog(context);
        final LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        listDialog.setContentView(R.layout.cust_main_account_leverage_dialog);
        listDialog.setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = listDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.copyFrom(window.getAttributes());


        listDialog.getWindow().setGravity(Gravity.BOTTOM);
        listDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

        ImageView mAccountInfoImg=(ImageView)listDialog.findViewById(R.id.img_account_info);
        TextView mChangeLeverageTv=(TextView)listDialog.findViewById(R.id.tv_change_leverage);
        TextView mResetTradePwdTv=(TextView)listDialog.findViewById(R.id.tv_reset_trade_pwd);

        mAccountInfoImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountDetailsInfoMainFragment fragment = new AccountDetailsInfoMainFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).addToBackStack(null).commit();
                listDialog.dismiss();
            }
        });

        mChangeLeverageTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeLeverageOfAccountFragment fragment = new ChangeLeverageOfAccountFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).addToBackStack(null).commit();
                listDialog.dismiss();
            }
        });

        mResetTradePwdTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                warning("3004541");
            }
        });


        listDialog.show();
    }

    public void warning(final String header) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Reset Trading Password");
        alertDialogBuilder.setMessage(context.getResources().getString(R.string.Are_you_sure_you_want_to_reset_password_for_trading_password) + " " + header + "?");
        // set positive button: Yes message
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {



                    captchaCodeDialog(context, header);



            }
        });
        // set negative button: No message
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // cancel the alert box and put a Toast to the user
                dialog.cancel();

            }
        });
        // set neutral button: Exit the app message

        AlertDialog alertDialog = alertDialogBuilder.create();
        // show alert
        alertDialog.show();

        //Toast.makeText(MainActivity.this,"logout",Toast.LENGTH_LONG).show();

    }


    /**
     * OLD ONE
     * dialog for get the captcha code
     */
    private void captchaCodeDialog(final Context context, final String value) {

        captchadialog = new Dialog(context);
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        captchadialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        captchadialog.setContentView(R.layout.dialog_captcha_code);
        captchadialog.setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = captchadialog.getWindow();
        lp.copyFrom(window.getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        /*getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        lp.width = width - 100;
        lp.height = height - 20;*/
        window.setAttributes(lp);

        captchadialog.getWindow().setGravity(Gravity.CENTER);
        captchadialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));


        mCaptchaImg = (ImageView) captchadialog.findViewById(R.id.img_captchacode);
        final EditText mCaptchaCodeEdt = (EditText) captchadialog.findViewById(R.id.edt_captchacode);
        TextView mSendTv = (TextView) captchadialog.findViewById(R.id.tv_submit_qrcode);
        TextView mCloseTv=(TextView)captchadialog.findViewById(R.id.tv_close_qrcode);
        mProgressbar = (ProgressBar) captchadialog.findViewById(R.id.pbProgress);



        mCloseTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                captchadialog.dismiss();
            }
        });
        mSendTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mCaptchaCodeEdt.getText().toString())) {
                    Toast.makeText(context, "Please check your email for new resetted trading account details.", Toast.LENGTH_SHORT).show();
                    captchadialog.dismiss();

                } else {
                   // mValidator.showtoast("Please enter captcha code.");
                    Toast.makeText(context, "Please provide captcha code.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        captchadialog.show();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                            fm.popBackStack();

                        }
                        MyProfileFragment fragment = new MyProfileFragment();
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).commit();
                        return true;
                    }
                }
                return false;
            }
        });
    }


}
