package com.uwfx.adapter;

/**
 * Created by aipxperts on 3/2/16.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.uwfx.R;

import java.util.ArrayList;

public class NavDrawerListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> navDrawerItems;
    private ArrayList<Integer> navDrawerItems_icons;

    public NavDrawerListAdapter(Context context, ArrayList<String> navDrawerItems , ArrayList<Integer> navDrawerItems_icons){
        this.context = context;
        this.navDrawerItems = navDrawerItems;
        this.navDrawerItems_icons=navDrawerItems_icons;

    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.drawer_list, null);

        }

        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        ImageView img_icon = (ImageView) convertView.findViewById(R.id.img_icon);
        TextView tv_right_message=(TextView)convertView.findViewById(R.id.tv_right_message);
       // txtTitle.setTypeface(FontCustom.setFontcontent(context));
        txtTitle.setText(navDrawerItems.get(position));
      //  img_icon.setImageResource(navDrawerItems_icons.get(position));

        if(position==8){
            tv_right_message.setVisibility(View.VISIBLE);
        }
        else{
            tv_right_message.setVisibility(View.GONE);
        }

        return convertView;
    }

}