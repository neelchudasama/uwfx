package com.uwfx.adapter;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.uwfx.R;
import com.uwfx.databinding.RowDepositwithdrawalStatusListBinding;
import com.uwfx.databinding.RowTransactionHistoryDepositwithdrawalListBinding;


/**
 * Created by aipxperts on 17/3/17.
 */

public class DepositWithdrawalStatusListAdapter extends BaseAdapter {

    Context context;
    Activity ac;

    public DepositWithdrawalStatusListAdapter(Activity activity) {
        context = activity;


    }
    /*public IdentityResidentialProfileListAdapter(Activity activity, ArrayList<FacebookuserzeebaListModel> payLoadList) {
        context = activity;
        this.payLoadList = payLoadList;


    }*/

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final RowDepositwithdrawalStatusListBinding mBinding;

        if (convertView == null) {

            mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_depositwithdrawal_status_list, parent, false);
            convertView = mBinding.getRoot();
            convertView.setTag(mBinding);
        } else {
            mBinding = (RowDepositwithdrawalStatusListBinding) convertView.getTag();
        }

        mBinding.imgDetailsHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.lnDetailOfHistory.setVisibility(View.VISIBLE);
            }
        });


        return convertView;
    }


}