package com.uwfx.adapter;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.uwfx.R;
import com.uwfx.databinding.RowIdentityResidentialProfileListBinding;


/**
 * Created by aipxperts on 17/3/17.
 */

public class IdentityResidentialProfileListAdapter extends BaseAdapter {

    Context context;
    Activity ac;


    public IdentityResidentialProfileListAdapter(Activity activity) {
        context = activity;


    }
    /*public IdentityResidentialProfileListAdapter(Activity activity, ArrayList<FacebookuserzeebaListModel> payLoadList) {
        context = activity;
        this.payLoadList = payLoadList;


    }*/

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        RowIdentityResidentialProfileListBinding mBinding;

        if (convertView == null) {

            mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_identity_residential_profile_list, parent, false);
            convertView = mBinding.getRoot();
            convertView.setTag(mBinding);
        } else {
            mBinding = (RowIdentityResidentialProfileListBinding) convertView.getTag();
        }

        if(position==9){
            mBinding.cardView.setVisibility(View.VISIBLE);
        }
        else{
            mBinding.cardView.setVisibility(View.GONE);
        }
        mBinding.lnIdentityMain.setVisibility(View.VISIBLE);

        return convertView;
    }

}