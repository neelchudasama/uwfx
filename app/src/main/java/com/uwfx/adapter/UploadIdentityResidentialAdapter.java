package com.uwfx.adapter;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.uwfx.R;
import com.uwfx.databinding.RowIdentityResidentialProfileListBinding;
import com.uwfx.databinding.RowUploadIdentityResidentialDocumentListBinding;


/**
 * Created by aipxperts on 17/3/17.
 */

public class UploadIdentityResidentialAdapter extends BaseAdapter {

    Context context;
    Activity ac;


    public UploadIdentityResidentialAdapter(Activity activity) {
        context = activity;


    }
    /*public IdentityResidentialProfileListAdapter(Activity activity, ArrayList<FacebookuserzeebaListModel> payLoadList) {
        context = activity;
        this.payLoadList = payLoadList;


    }*/

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        RowUploadIdentityResidentialDocumentListBinding mBinding;

        if (convertView == null) {

            mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_upload_identity_residential_document_list, parent, false);
            convertView = mBinding.getRoot();
            convertView.setTag(mBinding);
        } else {
            mBinding = (RowUploadIdentityResidentialDocumentListBinding) convertView.getTag();
        }


        return convertView;
    }

}