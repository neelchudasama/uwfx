package com.uwfx.ui.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.uwfx.R;
import com.uwfx.databinding.ActivityForgotPasswordBinding;
import com.uwfx.databinding.ActivityVerificationCodeBinding;

import java.util.ArrayList;

public class VerificationCodeActivity extends BaseActivity {

    private ActivityVerificationCodeBinding mBinding;
    boolean isValidation = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            mBinding = DataBindingUtil.setContentView(this, R.layout.activity_verification_code);


        setup();


    }

    private void setup() {





        mBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isValidation = true;

                if (TextUtils.isEmpty(mBinding.edtVerificationCode.getText().toString())) {
                    isValidation = false;
                    mBinding.edtVerificationCode.setError(getString(R.string.Please_provide_verification_code));
                }


                if (TextUtils.isEmpty(mBinding.edtNewCabinetPwd.getText().toString())) {
                    isValidation = false;
                    mBinding.edtNewCabinetPwd.setError(getString(R.string.Please_provide_new_cabinet_password));
                }

                if (mBinding.edtNewCabinetPwd.getText().toString().length() >= 1 && mBinding.edtNewCabinetPwd.getText().toString().length() < 6) {
                    isValidation = false;
                    mBinding.edtNewCabinetPwd.setError(getString(R.string.New_cabinet_password_length_within_6_to_15_characters));
                }

                if (TextUtils.isEmpty(mBinding.edtConfirmNewCabinetPwd.getText().toString())) {
                    isValidation = false;
                    mBinding.edtConfirmNewCabinetPwd.setError(getString(R.string.Please_provide_confirm_new_cabinet_password));
                }

                if (mBinding.edtConfirmNewCabinetPwd.getText().toString().length() >= 1 && !mBinding.edtNewCabinetPwd.getText().toString().equals(mBinding.edtConfirmNewCabinetPwd.getText().toString())) {
                    isValidation = false;
                    mBinding.edtConfirmNewCabinetPwd.setError(getString(R.string.New_cabinet_password_not_matched));
                }



                if (isValidation) {
                    Log.e("yyy", "Validation");
                    Toast.makeText(VerificationCodeActivity.this, "Your cabinet password has been reset successfully.", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(VerificationCodeActivity.this,MainActivity.class));
                    finish();
                }

            }
        });


        mBinding.imgBackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



    }



}
