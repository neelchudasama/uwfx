package com.uwfx.ui.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.uwfx.R;
import com.uwfx.databinding.ActivitySignaupStep1Binding;
import com.uwfx.databinding.ActivitySignaupStep2Binding;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class SignaupStep2Activity extends BaseActivity {

    ActivitySignaupStep2Binding mBinding;
    boolean isValidation = true;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_signaup_step2);

        setup();
    }

    private void setup() {

        setAccountLeverageAndBonusList();

        context = SignaupStep2Activity.this;


        mBinding.tlCabinetPwd.setHint("Cabinet Password");
        mBinding.tlConfirmCabinetPwd.setHint("Confirm Cabinet Password");
        mBinding.tlWithdrawalpinNo.setHint("Withdrawal PIN No.");
        mBinding.tlConfirmWithdrawalpinNo.setHint("Confirm Withdrawal PIN No.");

        mBinding.imgBackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mBinding.txtNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isValidation = true;

                if (mBinding.spAccountTypeList.getSelectedItemPosition() == 0) {
                    isValidation = false;
                    mBinding.tvAccountType.setError(getString(R.string.Please_select_account_type));
                } else {
                    mBinding.tvAccountType.setError(null);
                }
                if (mBinding.spLverageList.getSelectedItemPosition() == 0) {
                    isValidation = false;
                    mBinding.tvLeverage.setError(getString(R.string.Please_select_leverage));

                } else {
                    mBinding.tvLeverage.setError(null);
                }

                if (mBinding.spBonusOptionList.getSelectedItemPosition() == 0) {
                    isValidation = false;
                    mBinding.tvBonusOption.setError(getString(R.string.Please_select_bonus));

                } else {
                    mBinding.tvBonusOption.setError(null);
                }

                if (mBinding.cbTermsAndCondition.isChecked() == false) {
                    isValidation = false;
                    Toast.makeText(context, "Please agree to bonus Terms and Conditions.", Toast.LENGTH_SHORT).show();
                } else {

                }

                if (TextUtils.isEmpty(mBinding.edtCabinetPwd.getText().toString())) {
                    isValidation = false;
                    mBinding.edtCabinetPwd.setError(getString(R.string.Please_provide_cabinet_password));
                }

                if (mBinding.edtCabinetPwd.getText().toString().length() >= 1 && mBinding.edtCabinetPwd.getText().toString().length() < 6) {
                    isValidation = false;
                    mBinding.edtCabinetPwd.setError(getString(R.string.Cabinet_password_length_within_6_to_15_characters));
                }

                if (TextUtils.isEmpty(mBinding.edtConfirmCabinetPwd.getText().toString())) {
                    isValidation = false;
                    mBinding.edtConfirmCabinetPwd.setError(getString(R.string.Please_provide_confirm_cabinet_password));
                }

                if (mBinding.edtConfirmCabinetPwd.getText().toString().length() >= 1 && !mBinding.edtCabinetPwd.getText().toString().equals(mBinding.edtConfirmCabinetPwd.getText().toString())) {
                    isValidation = false;
                    mBinding.edtConfirmCabinetPwd.setError(getString(R.string.Cabinet_password_not_matched));
                }

                if (TextUtils.isEmpty(mBinding.edtWithdrawalpinNo.getText().toString())) {
                    isValidation = false;
                    mBinding.edtWithdrawalpinNo.setError(getString(R.string.Please_provide_withdrawal_pin_no));
                }

                if (mBinding.edtWithdrawalpinNo.getText().toString().length() >= 1 && mBinding.edtWithdrawalpinNo.getText().toString().length() < 6) {
                    isValidation = false;
                    mBinding.edtWithdrawalpinNo.setError(getString(R.string.Withdrawal_pin_no_length_within_6_to_15_characters));
                }

                if (TextUtils.isEmpty(mBinding.edtConfirmWithdrawalpinNo.getText().toString())) {
                    isValidation = false;
                    mBinding.edtConfirmWithdrawalpinNo.setError(getString(R.string.Please_provide_confirm_withdrawal_pin_no));
                }

                if (mBinding.edtConfirmWithdrawalpinNo.getText().toString().length() >= 1 && !mBinding.edtWithdrawalpinNo.getText().toString().equals(mBinding.edtConfirmWithdrawalpinNo.getText().toString())) {
                    isValidation = false;
                    mBinding.edtConfirmWithdrawalpinNo.setError(getString(R.string.Withdrawal_pin_no_not_matched));
                }

                if (isValidation) {
                    Log.e("yyy", "Validation");
                    startActivity(new Intent(SignaupStep2Activity.this, SignaupStep3FinalActivity.class));
                }

            }
        });


    }


    private void setAccountLeverageAndBonusList() {

        mBinding.tvAccountType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBinding.spAccountTypeList.performClick();
            }
        });

        mBinding.tvLeverage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.spLverageList.performClick();

            }
        });

        mBinding.tvBonusOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.spBonusOptionList.performClick();

            }
        });

        final ArrayList<String> countryList = new ArrayList<>();
        final ArrayList<String> nationalityList = new ArrayList<>();
        final ArrayList<String> bonusList = new ArrayList<>();

        countryList.add("Account Type");
        for (int i = 0; i < 10; i++) {
            countryList.add("" + i);
        }

        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, countryList);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBinding.spAccountTypeList.setAdapter(aa);

        nationalityList.add("Leverage");
        for (int i = 0; i < 10; i++) {
            nationalityList.add("" + i);
        }


        ArrayAdapter aa1 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, nationalityList);
        aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBinding.spLverageList.setAdapter(aa1);


        bonusList.add("Bonus");
        for (int i = 0; i < 10; i++) {
            bonusList.add("" + i);
        }

        ArrayAdapter aa2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, bonusList);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBinding.spBonusOptionList.setAdapter(aa2);


        mBinding.spAccountTypeList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mBinding.tvAccountType.setText(countryList.get(position).toString());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mBinding.spLverageList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mBinding.tvLeverage.setText(nationalityList.get(position).toString());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mBinding.spBonusOptionList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mBinding.tvBonusOption.setText(bonusList.get(position).toString());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

}
