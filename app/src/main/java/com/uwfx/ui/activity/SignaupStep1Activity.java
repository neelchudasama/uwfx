package com.uwfx.ui.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;

import com.uwfx.R;
import com.uwfx.databinding.ActivitySignaupStep1Binding;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class SignaupStep1Activity extends BaseActivity {

    ActivitySignaupStep1Binding mBinding;
    boolean isValidation = true;
    Context context;

    // calendar

    private Calendar cal;
    private int day;
    private int month;
    private int year;

    private boolean is18;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_signaup_step1);

        setup();
    }

    private void setup() {

        setCountryAndNationalityList();

        context = SignaupStep1Activity.this;

        cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH) + 1;
        year = cal.get(Calendar.YEAR);

        mBinding.tlFullName.setHint("Full Name");
        mBinding.tlIdentificationNumber.setHint("Identification Number");
        mBinding.edDateOfBirth.setHint("Date of Birth");
        mBinding.tlAddress.setHint("Resideatial Address");
        mBinding.tlPostalCode.setHint("Postal / Zip Code");
        mBinding.tlState.setHint("State");

        mBinding.imgBackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mBinding.txtNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isValidation = true;
                if (TextUtils.isEmpty(mBinding.edname.getText().toString())) {
                    isValidation = false;
                    mBinding.edname.setError(getString(R.string.full_name_is_empty));
                }
                if (TextUtils.isEmpty(mBinding.edIdentityNumber.getText().toString())) {
                    isValidation = false;
                    mBinding.edIdentityNumber.setError(getString(R.string.please_provide_identification_number));
                }
                if (TextUtils.isEmpty(mBinding.edDateOfBirth.getText().toString())) {
                    isValidation = false;
                    mBinding.edDateOfBirth.setError(getString(R.string.please_provide_date_of_birth));
                } else {
                    if (!is18) {
                        isValidation = false;
                        mBinding.edDateOfBirth.setError(getString(R.string.age_must_be_18_year));
                    } else {
                        mBinding.edDateOfBirth.setError(null);
                    }
                }
                if (TextUtils.isEmpty(mBinding.edAddress.getText().toString())) {
                    isValidation = false;
                    mBinding.edAddress.setError(getString(R.string.please_provide_address));
                }
                if (TextUtils.isEmpty(mBinding.edPostCode.getText().toString())) {
                    isValidation = false;
                    mBinding.edPostCode.setError(getString(R.string.please_provide_postal_code));
                }
                if (TextUtils.isEmpty(mBinding.edState.getText().toString())) {
                    isValidation = false;
                    mBinding.edState.setError(getString(R.string.please_provide_state));
                }
                if (mBinding.spNationality.getSelectedItemPosition() == 0) {
                    isValidation = false;
                    mBinding.txtNationality.setError(getString(R.string.please_select_nationality));
                } else {
                    mBinding.txtNationality.setError(null);
                }
                if (mBinding.spContryList.getSelectedItemPosition() == 0) {
                    isValidation = false;
                    mBinding.txtCountryName.setError(getString(R.string.please_select_country));

                } else {
                    mBinding.txtCountryName.setError(null);
                }

                if (isValidation) {
                    Log.e("yyy", "Validation");
                    startActivity(new Intent(SignaupStep1Activity.this,SignaupStep2Activity.class));
                }

            }
        });

        mBinding.edDateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DateDialog();
            }
        });
    }

    public void DateDialog() {

        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                mBinding.edDateOfBirth.setText(dayOfMonth + "/" + monthOfYear + "/" + year);

                Calendar userAge = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                Calendar minAdultAge = new GregorianCalendar();

                minAdultAge.add(Calendar.YEAR, -18);

                if (minAdultAge.before(userAge)) {
                    is18 = false;
                } else {
                    is18 = true;
                }


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(this, listener, year, month, day);
        dpDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

        dpDialog.show();
    }

    private void setCountryAndNationalityList() {

        mBinding.txtCountryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBinding.spContryList.performClick();
            }
        });

        mBinding.txtNationality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.spNationality.performClick();

            }
        });


        final ArrayList<String> countryList = new ArrayList<>();
        final ArrayList<String> nationalityList = new ArrayList<>();

        countryList.add("Select Country");
        for (int i = 0; i < 10; i++) {
            countryList.add("Country :" + i);
        }

        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, countryList);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBinding.spContryList.setAdapter(aa);

        nationalityList.add("Select Nationality");
        for (int i = 0; i < 10; i++) {
            nationalityList.add("Nationality :" + i);
        }


        ArrayAdapter aa1 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, nationalityList);
        aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBinding.spNationality.setAdapter(aa1);

        mBinding.spContryList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mBinding.txtCountryName.setText(countryList.get(position).toString());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mBinding.spNationality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mBinding.txtNationality.setText(nationalityList.get(position).toString());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

}
