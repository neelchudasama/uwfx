package com.uwfx.ui.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.uwfx.R;
import com.uwfx.adapter.NavDrawerListAdapter;
import com.uwfx.databinding.ActivityDashboardBinding;
import com.uwfx.fragment.ChangePasswordFragment;
import com.uwfx.fragment.ContactUsMainFragment;
import com.uwfx.fragment.CreateNewTradeAccountFragment;
import com.uwfx.fragment.MyAccountMainFragment;
import com.uwfx.fragment.MyProfileFragment;
import com.uwfx.fragment.MyWalletMainFragment;
import com.uwfx.fragment.ParternershipMainFragment;
import com.uwfx.fragment.ShareAppMainFragment;
import com.uwfx.fragment.TransactionHistoryDepositWithdrawalFragment;
import com.uwfx.fragment.UploadDocumentsFragment;
import com.uwfx.fragment.WebViewForAboutusFragment;
import com.uwfx.utils.Sample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class DashboardActivity extends FragmentActivity {

    ActivityDashboardBinding mBinding;
    ArrayList<String> navDrawerItems;
    ArrayList<Integer> navDrawerItems_icons;
    private ActionBarDrawerToggle mDrawerToggle;
    Dialog profilePoupDialog;
    Point p;
    Dialog languageDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);

        prepareView();
        setOnClickListener();
        setdrawer();
        mBinding.listSlidemenuItem.setOnItemClickListener(new SlideMenuClickListener());


        setDrawerSlidewithView();

        MyProfileFragment fragment = new MyProfileFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).commit();
    }


    private void prepareView() {

    }

    private void setOnClickListener() {
        mBinding.llMainMyProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyProfileFragment fragment = new MyProfileFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).commit();
            }
        });

        mBinding.llMainMyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyAccountMainFragment fragment = new MyAccountMainFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).addToBackStack(null).commit();
            }
        });


        mBinding.llMainWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyWalletMainFragment fragment = new MyWalletMainFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).addToBackStack(null).commit();
            }
        });

        mBinding.llMainPartership.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParternershipMainFragment fragment = new ParternershipMainFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).addToBackStack(null).commit();
            }
        });

        mBinding.llMainContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsMainFragment fragment = new ContactUsMainFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).addToBackStack(null).commit();
            }
        });

        //for view all click..

        mBinding.header.imgMenuOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getCurrentFragment() instanceof MyProfileFragment) {
                    confirmationforChallengeGame(DashboardActivity.this);
                }
            }
        });

        mBinding.header.tvRightTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getCurrentFragment() instanceof MyAccountMainFragment) {
                    CreateNewTradeAccountFragment fragment = new CreateNewTradeAccountFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).addToBackStack(null).commit();
                } else if (getCurrentFragment() instanceof MyWalletMainFragment) {
                    TransactionHistoryDepositWithdrawalFragment fragment = new TransactionHistoryDepositWithdrawalFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).addToBackStack(null).commit();
                }
            }
        });

        mBinding.header.imgBackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                fm.popBackStack();
            }
        });
    }

    private void confirmationforChallengeGame(Context context) {

        profilePoupDialog = new Dialog(context);
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        profilePoupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        profilePoupDialog.setContentView(R.layout.profile_popup_menu);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(profilePoupDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        profilePoupDialog.show();

        profilePoupDialog.getWindow().setAttributes(lp);

        profilePoupDialog.getWindow().setGravity(Gravity.RIGHT | Gravity.TOP);
        profilePoupDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

        TextView mUploadDucumentTv = (TextView) profilePoupDialog.findViewById(R.id.tv_upload_document);
        TextView mChangePasswordTv = (TextView) profilePoupDialog.findViewById(R.id.tv_change_pwd);

        mUploadDucumentTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profilePoupDialog.dismiss();
                UploadDocumentsFragment fragment = new UploadDocumentsFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).addToBackStack(null).commit();
            }
        });


        mChangePasswordTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profilePoupDialog.dismiss();
                ChangePasswordFragment fragment = new ChangePasswordFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).addToBackStack(null).commit();
            }
        });

        profilePoupDialog.show();
    }


    private void setDrawerSlidewithView() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mBinding.drawerLayout, R.mipmap.ic_launcher, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                mBinding.mainView.setTranslationX(slideOffset * drawerView.getWidth());
                mBinding.header.toolbar.setTranslationX(slideOffset * drawerView.getWidth());
                mBinding.drawerLayout.bringChildToFront(drawerView);
                mBinding.drawerLayout.requestLayout();
            }
        };
        mBinding.drawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * Slide menu item click listener
     */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }

    public void setdrawer() {
        navDrawerItems = new ArrayList<String>();
        navDrawerItems.add("Menu");
        navDrawerItems.add("About Us");
        navDrawerItems.add("UWFX Regulation");
        navDrawerItems.add("Client Agreement");
        navDrawerItems.add("Anti-Money Laundering Policy");
        navDrawerItems.add("KYC Policies and Procedures");
        navDrawerItems.add("Risk Warning");
        navDrawerItems.add("Privacy Policy");
        navDrawerItems.add("Language");
        navDrawerItems.add("Share App");
        navDrawerItems.add("Sign Out");

        navDrawerItems_icons = new ArrayList<Integer>();
        navDrawerItems_icons.add(R.mipmap.home);
        navDrawerItems_icons.add(R.mipmap.home);


        final NavDrawerListAdapter adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems, navDrawerItems_icons);
        mBinding.listSlidemenuItem.setAdapter(adapter);
        mBinding.header.imgMenuDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBinding.drawerLayout.openDrawer(Gravity.LEFT);


            }
        });

    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     */
    private void displayView(int position) {
        // update the main content by replacing fragments

        switch (position) {

            case 0:

                MyProfileFragment fragment = new MyProfileFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment).commit();

                mBinding.drawerLayout.closeDrawer(Gravity.LEFT);
                break;
            case 1:

                WebViewForAboutusFragment webViewForAboutusFragment = new WebViewForAboutusFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, webViewForAboutusFragment).addToBackStack(null).commit();

                mBinding.drawerLayout.closeDrawer(Gravity.LEFT);


                break;

            case 2:
                WebViewForAboutusFragment webViewForAboutusFragment2 = new WebViewForAboutusFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, webViewForAboutusFragment2).addToBackStack(null).commit();

                mBinding.drawerLayout.closeDrawer(Gravity.LEFT);


                break;

            case 3:

                WebViewForAboutusFragment webViewForAboutusFragment3 = new WebViewForAboutusFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, webViewForAboutusFragment3).addToBackStack(null).commit();

                mBinding.drawerLayout.closeDrawer(Gravity.LEFT);
                break;

            case 4:


                mBinding.drawerLayout.closeDrawer(Gravity.LEFT);

                ShareAppMainFragment fragment1 = new ShareAppMainFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, fragment1).addToBackStack(null).commit();
                mBinding.drawerLayout.closeDrawer(Gravity.LEFT);
                break;

            case 5:

                WebViewForAboutusFragment webViewForAboutusFragment5 = new WebViewForAboutusFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, webViewForAboutusFragment5).addToBackStack(null).commit();

                mBinding.drawerLayout.closeDrawer(Gravity.LEFT);
                break;
            case 6:

                WebViewForAboutusFragment webViewForAboutusFragment6 = new WebViewForAboutusFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, webViewForAboutusFragment6).addToBackStack(null).commit();

                mBinding.drawerLayout.closeDrawer(Gravity.LEFT);
                break;
            case 7:

                WebViewForAboutusFragment webViewForAboutusFragment7 = new WebViewForAboutusFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, webViewForAboutusFragment7).addToBackStack(null).commit();
                mBinding.drawerLayout.closeDrawer(Gravity.LEFT);

                break;
            case 8:

                languageChangeDialog(DashboardActivity.this, "english");


                break;

            case 9:

                WebViewForAboutusFragment webViewForAboutusFragment9 = new WebViewForAboutusFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, webViewForAboutusFragment9).addToBackStack(null).commit();

                mBinding.drawerLayout.closeDrawer(Gravity.LEFT);
                break;

            case 10:


                warning("");
                break;

            default:
                break;

        }

    }

    public void warning(final String header) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DashboardActivity.this);
        alertDialogBuilder.setTitle("UWForex");
        alertDialogBuilder.setMessage("Are you sure you want to sign out?");
        // set positive button: Yes message
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


                Toast.makeText(DashboardActivity.this, "Logout successfully!", Toast.LENGTH_SHORT).show();
                dialog.cancel();
                mBinding.drawerLayout.closeDrawer(Gravity.LEFT);



            }
        });
        // set negative button: No message
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // cancel the alert box and put a Toast to the user
                dialog.cancel();

            }
        });
        // set neutral button: Exit the app message

        AlertDialog alertDialog = alertDialogBuilder.create();
        // show alert
        alertDialog.show();

        //Toast.makeText(MainActivity.this,"logout",Toast.LENGTH_LONG).show();

    }

    /**
     * dialog for get the languageChangeDialog code
     */
    private void languageChangeDialog(final Context context, final String value) {

        languageDialog = new Dialog(context);
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        languageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        languageDialog.setContentView(R.layout.dialog_language_change);
        languageDialog.setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = languageDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        /*getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        lp.width = width - 100;
        lp.height = height - 20;*/
        window.setAttributes(lp);

        languageDialog.getWindow().setGravity(Gravity.CENTER);
        languageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));


        final RadioButton rbEng = (RadioButton) languageDialog.findViewById(R.id.rb_eng);
        final RadioButton rbChinese = (RadioButton) languageDialog.findViewById(R.id.rb_chinese);
        final TextView tvEng = (TextView) languageDialog.findViewById(R.id.tv_eng);
        final TextView tvChinese = (TextView) languageDialog.findViewById(R.id.tv_chinese);

        rbEng.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (rbEng.isChecked() && isChecked) {
                    rbChinese.setChecked(false);
                }
                tvEng.setTypeface(null, Typeface.BOLD);
                tvEng.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                tvChinese.setTypeface(null, Typeface.NORMAL);
                tvChinese.setTextColor(ContextCompat.getColor(context, R.color.gray));
                // rbEng.setEnabled(isChecked);
            }
        });
        rbChinese.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (rbChinese.isChecked() && isChecked) {
                    rbEng.setChecked(false);
                }
                tvChinese.setTypeface(null, Typeface.BOLD);
                tvChinese.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                tvEng.setTypeface(null, Typeface.NORMAL);
                tvEng.setTextColor(ContextCompat.getColor(context, R.color.gray));
                // rbChinese.setEnabled(isChecked);
            }
        });


        languageDialog.show();
    }

    public void visibleMenuBack(String frag) {
        if (frag.equals("main")) {
            mBinding.header.imgMenuDrawer.setVisibility(View.VISIBLE);
            mBinding.header.imgBackView.setVisibility(View.GONE);
        } else {
            mBinding.header.imgMenuDrawer.setVisibility(View.GONE);
            mBinding.header.imgBackView.setVisibility(View.VISIBLE);
        }

    }

    public void visibleRighttitle(String title) {
        mBinding.header.tvRightTitle.setText(title);
        mBinding.header.tvRightTitle.setVisibility(View.VISIBLE);
    }


    public void gonRightTitle() {
        mBinding.header.tvRightTitle.setVisibility(View.GONE);
    }

    public void visibleRightImage(int image) {
        mBinding.header.imgMenuOption.setImageResource(image);
        mBinding.header.imgMenuOption.setVisibility(View.VISIBLE);
    }


    public void goneRightImage() {
        mBinding.header.imgMenuOption.setVisibility(View.GONE);
    }

    public void setHeaderTitle(String title) {
        mBinding.header.tvViewTitle.setText(title);
    }

    public Fragment getCurrentFragment() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.frame_main_container);
        return currentFragment;

    }


}
