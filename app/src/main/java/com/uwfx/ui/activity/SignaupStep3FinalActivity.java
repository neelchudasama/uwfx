package com.uwfx.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.uwfx.R;
import com.uwfx.databinding.ActivitySignaupStep2Binding;
import com.uwfx.databinding.ActivitySignaupStep3FinalBinding;
import com.uwfx.utils.Pref;

import java.util.ArrayList;

public class SignaupStep3FinalActivity extends BaseActivity {

    ActivitySignaupStep3FinalBinding mBinding;
    boolean isValidation = true;
    Context context;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_signaup_step3_final);

        if(!Pref.getValue(SignaupStep3FinalActivity.this,"AGENT_QR_CODE","").isEmpty()){
            mBinding.edtAgentCode.setText(Pref.getValue(SignaupStep3FinalActivity.this,"AGENT_QR_CODE",""));
            mBinding.edtAgentCode.setEnabled(false);
        }
        else{
            mBinding.edtAgentCode.setEnabled(true);
        }

        setup();
    }

    private void setup() {
        context = SignaupStep3FinalActivity.this;


        mBinding.imgBackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        mBinding.txtNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isValidation = true;


                if(mBinding.cbTermsAndCondition.isChecked()==false){
                    isValidation = false;
                    Toast.makeText(context, "Please agree to terms and conditions.", Toast.LENGTH_SHORT).show();
                }
                else{

                }

                /*if (TextUtils.isEmpty(mBinding.edtAgentCode.getText().toString())) {
                    isValidation = false;
                    mBinding.edtAgentCode.setError(getString(R.string.full_name_is_empty));
                }*/





                if (isValidation) {
                    Log.e("yyy", "Validation");
                    Toast.makeText(context, "Welcome to UW. Your trading account details has been sent to your email.", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(SignaupStep3FinalActivity.this,DashboardActivity.class));
                }

            }
        });


    }







}
