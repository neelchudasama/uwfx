package com.uwfx.ui.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;

import com.uwfx.R;
import com.uwfx.databinding.ActivitySignaUpVerificationBinding;

public class SignaUpVerificationActivity extends BaseActivity {

    ActivitySignaUpVerificationBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_signa_up_verification);

        setup();
    }

    private void setup() {

        displayCountDown();

        mBinding.imgBackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mBinding.txtCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(mBinding.edVerificationCode.getText().toString())) {
                    customToast(getString(R.string.please_provide_verification_code), R.mipmap.green_yes);
                    return;
                }

                Intent i = new Intent(SignaUpVerificationActivity.this, SignaupStep1Activity.class);
                startActivity(i);
                finish();
            }
        });
    }

    private void displayCountDown() {

        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                mBinding.txtResent.setText("" + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                mBinding.txtResent.setText("Resend verification code");
            }

        }.start();
    }
}
