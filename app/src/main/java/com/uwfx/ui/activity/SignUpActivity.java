package com.uwfx.ui.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.uwfx.R;
import com.uwfx.databinding.ActivitySignUpPage1Binding;

import java.util.ArrayList;

public class SignUpActivity extends BaseActivity {

    private ActivitySignUpPage1Binding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up_page1);


        setup();


    }

    private void setup() {
        mBinding.tlEmail.setHint(getString(R.string.email_address));
        mBinding.tlEmail2.setHint(getString(R.string.confirm_email_address));

        setCountryList();

        mBinding.imgBackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mBinding.edEmail1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!TextUtils.isEmpty(s)) {
                    mBinding.spContryList.setSelection(0);
                    mBinding.edPhonNo.setText("");

                    mBinding.txtCountryName.setError(null);
                    mBinding.edPhonNo.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mBinding.edEmail1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s)) {
                    mBinding.spContryList.setSelection(0);
                    mBinding.edPhonNo.setText("");

                    mBinding.txtCountryName.setError(null);
                    mBinding.edPhonNo.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mBinding.edPhonNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s)) {
                    mBinding.edEmail1.setText("");
                    mBinding.edEmail2.setText("");

                    mBinding.edEmail1.setError(null);
                    mBinding.edEmail2.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mBinding.txtSendVarification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mBinding.spContryList.getSelectedItemPosition() == 0) { // email

                    mBinding.txtCountryName.setError(null);
                    mBinding.edPhonNo.setError(null);

                    boolean isEmail = checkEmail1();
                    boolean isEmail2 = checkEmail2();

                    if (isEmail && isEmail2) {
                        Intent i = new Intent(SignUpActivity.this, SignaUpVerificationActivity.class);
                        startActivity(i);
                    }
                } else { // phone no

                    mBinding.edEmail1.setError(null);
                    mBinding.edEmail2.setError(null);

                    boolean checkCountry = checkCountry();
                    boolean checkPhonoNo = checkPhonoNo();

                    if (checkCountry && checkPhonoNo) {
                        confirmDialog();

                    }
                }


            }
        });

        mBinding.txtCountryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBinding.spContryList.performClick();
            }
        });

    }

    private void setCountryList() {
        final ArrayList<String> countryList = new ArrayList<>();

        countryList.add("Select Country");
        for (int i = 0; i < 10; i++) {
            countryList.add("Country :" + i);
        }

        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, countryList);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBinding.spContryList.setAdapter(aa);

        mBinding.spContryList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mBinding.txtCountryName.setText(countryList.get(position).toString());

                if (position != 0) {
                    mBinding.edEmail1.setText("");
                    mBinding.edEmail2.setText("");

                    mBinding.edEmail1.setError(null);
                    mBinding.edEmail2.setError(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public boolean checkEmail1() {
        if (TextUtils.isEmpty(mBinding.edEmail1.getText().toString())) {
            mBinding.edEmail1.setError(getString(R.string.please_provide_email_address));
            return false;
        } else if (!isValidEmail(mBinding.edEmail1.getText().toString())) {
            mBinding.edEmail1.setError(getString(R.string.please_enter_valid_email_Address));
            return false;
        } else {
            return true;
        }

    }

    public boolean checkEmail2() {

        if (TextUtils.isEmpty(mBinding.edEmail2.getText().toString())) {
            mBinding.edEmail2.setError(getString(R.string.please_provide_confirm_email_address));
            return false;
        } else if (!mBinding.edEmail1.getText().toString().equals(mBinding.edEmail2.getText().toString())) {
            mBinding.edEmail2.setError(getString(R.string.both_email_not_matched));
            return false;
        } else {
            return true;
        }
    }

    public boolean checkCountry() {
        if (mBinding.spContryList.getSelectedItemPosition() == 0) {
            mBinding.txtCountryName.setError(getString(R.string.please_select_country));
            return false;
        } else {
            mBinding.txtCountryName.setError(null);
            return true;
        }
    }

    public boolean checkPhonoNo() {

        if (TextUtils.isEmpty(mBinding.edPhonNo.getText().toString())) {
            mBinding.edPhonNo.setError(getString(R.string.please_provide_phone_no));
            return false;
        } else {
            return true;
        }
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private void confirmDialog() {
        final Dialog listDialog;
        listDialog = new Dialog(SignUpActivity.this);
        LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        listDialog.setContentView(R.layout.dialog_varification);
        listDialog.setCanceledOnTouchOutside(false);
        listDialog.setCancelable(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = listDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.copyFrom(window.getAttributes());


        listDialog.getWindow().setGravity(Gravity.CENTER);
        listDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));


        TextView txtYes, txtNo;

        txtYes = (TextView) listDialog.findViewById(R.id.txtYes);
        txtNo = (TextView) listDialog.findViewById(R.id.txtNo);

        txtYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listDialog.dismiss();

                customToast(getString(R.string.please_check_your_phone_for_varification), R.mipmap.green_yes);

                Intent i = new Intent(SignUpActivity.this, SignaUpVerificationActivity.class);
                startActivity(i);

            }


        });
        txtNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listDialog.dismiss();
            }
        });
        listDialog.show();
    }

}
