package com.uwfx.ui.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.uwfx.R;
import com.uwfx.databinding.ActivityForgotPasswordBinding;
import com.uwfx.databinding.ActivityLoginBinding;

import java.util.ArrayList;

public class ForgotPasswordActivity extends BaseActivity {

    private ActivityForgotPasswordBinding mBinding;
    boolean isValidation = true;
    Dialog captchadialog;
    ImageView mCaptchaImg;
    ProgressBar mProgressbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            mBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);


        setup();


    }

    private void setup() {

        setCountryList();

        mBinding.edtEmailAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!TextUtils.isEmpty(s)) {
                    mBinding.spContryList.setSelection(0);
                    mBinding.edPhonNo.setText("");

                    mBinding.txtCountryName.setError(null);
                    mBinding.edPhonNo.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        mBinding.edPhonNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s)) {
                    mBinding.edtEmailAddress.setText("");

                    mBinding.edtEmailAddress.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mBinding.btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isValidation = true;

                if(mBinding.edtEmailAddress.getText().length()==0) {
                    if (mBinding.spContryList.getSelectedItemPosition() == 0) {
                        isValidation = false;
                        mBinding.txtCountryName.setError(getString(R.string.please_select_country));
                    } else {
                        mBinding.txtCountryName.setError(null);
                    }

                    if (TextUtils.isEmpty(mBinding.edPhonNo.getText().toString())) {
                        isValidation = false;
                        mBinding.edPhonNo.setError(getString(R.string.please_provide_phone_no));
                    }

                }

                if(mBinding.edPhonNo.getText().length()==0){
                    if (TextUtils.isEmpty(mBinding.edtEmailAddress.getText().toString())) {
                        isValidation = false;
                        mBinding.edtEmailAddress.setError(getString(R.string.please_provide_email_address));
                    }

                    if (!isValidEmail(mBinding.edtEmailAddress.getText().toString())) {
                        isValidation = false;
                        mBinding.edtEmailAddress.setError(getString(R.string.please_enter_valid_email_Address));
                    }
                }


                if (TextUtils.isEmpty(mBinding.edtTradeAccount.getText().toString())) {
                    isValidation = false;
                    mBinding.edtTradeAccount.setError(getString(R.string.Please_provide_any_trading_account_under_your_cabinet_account));
                }

                if (isValidation) {
                    Log.e("yyy", "Validation");
                    if(mBinding.edPhonNo.getText().toString().length()>0){
                        captchaCodeDialog("phone");
                    }
                    else{
                        captchaCodeDialog("email");
                    }
                }

            }
        });

        mBinding.tvReceivedCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ForgotPasswordActivity.this, VerificationCodeActivity.class);
                startActivity(i);
            }
        });
        mBinding.txtCountryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBinding.spContryList.performClick();
            }
        });

        mBinding.imgBackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void setCountryList() {
        final ArrayList<String> countryList = new ArrayList<>();

        countryList.add("Select Country");
        for (int i = 0; i < 10; i++) {
            countryList.add("Country :" + i);
        }

        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, countryList);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBinding.spContryList.setAdapter(aa);

        mBinding.spContryList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mBinding.txtCountryName.setText(countryList.get(position).toString());

                if (position != 0) {
                    mBinding.edtEmailAddress.setText("");

                    mBinding.edtEmailAddress.setError(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    /**
     * OLD ONE
     * dialog for get the captcha code
     */
    private void captchaCodeDialog(final String value) {

        captchadialog = new Dialog(ForgotPasswordActivity.this);
        LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        captchadialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        captchadialog.setContentView(R.layout.dialog_captcha_code);
        captchadialog.setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = captchadialog.getWindow();
        lp.copyFrom(window.getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        lp.width = width - 100;
        lp.height = height - 20;
        window.setAttributes(lp);

        captchadialog.getWindow().setGravity(Gravity.CENTER);
        captchadialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

        mCaptchaImg = (ImageView) captchadialog.findViewById(R.id.img_captchacode);
        final EditText mCaptchaCodeEdt = (EditText) captchadialog.findViewById(R.id.edt_captchacode);
        mProgressbar = (ProgressBar) captchadialog.findViewById(R.id.pbProgress);
        TextView mSendTv = (TextView) captchadialog.findViewById(R.id.tv_submit_qrcode);
        TextView mCloseTv = (TextView) captchadialog.findViewById(R.id.tv_close_qrcode);

        mCloseTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                captchadialog.dismiss();
            }
        });
        mSendTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mCaptchaCodeEdt.getText().toString())) {
                    if(value.equals("email")){
                        Toast.makeText(ForgotPasswordActivity.this, "Please check your email for reset password instruction.", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(ForgotPasswordActivity.this,DashboardActivity.class));
                    }
                    else{
                        Toast.makeText(ForgotPasswordActivity.this, "Please check your phone for verification code.", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(ForgotPasswordActivity.this,VerificationCodeActivity.class));
                        finish();
                    }



                    captchadialog.dismiss();
                } else {
                    Toast.makeText(ForgotPasswordActivity.this, "Please enter captcha code.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        captchadialog.show();
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }



}
