package com.uwfx.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.uwfx.utils.ConnectionDetector;
import com.uwfx.utils.Pref;


import me.dm7.barcodescanner.core.CameraPreview;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by dharmesh on 11/8/16.
 */
public class ScanAgentQrCodeActivity extends Activity implements ZXingScannerView.ResultHandler {

    private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;


    private ZXingScannerView mScannerView;
    ConnectionDetector cd;
    String Qr_Code;

    private static final int MY_REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZXingScannerView(this);    // Programmatically initialize the scanner view
        setContentView(mScannerView);
        mScannerView.getFormats().add(BarcodeFormat.QR_CODE);
        //  setContentView(R.layout.activity_barcode_scanner);
        cd = new ConnectionDetector(ScanAgentQrCodeActivity.this);

    }

    @Override
    public void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        MY_REQUEST_CODE);
            } else {
                mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
                mScannerView.startCamera();          // Start camera on resume
            }
        } else {
            mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
            mScannerView.startCamera();          // Start camera on resume
        }


    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause

    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        //Log.v("res", rawResult.get); // Prints scan results
        Log.v("res", rawResult.getText() + ""); // Prints the scan format (qrcode, pdf417 etc.)

        Qr_Code = rawResult.getText();

        Toast.makeText(this, "Code " + Qr_Code, Toast.LENGTH_SHORT).show();
        Pref.setValue(ScanAgentQrCodeActivity.this,"AGENT_QR_CODE",Qr_Code);
        finish();

    }





    @Override
    public void onBackPressed() {
        super.onBackPressed();


    }
}

