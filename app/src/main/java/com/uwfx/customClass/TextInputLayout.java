package com.uwfx.customClass;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by aipxperts on 5/7/17.
 */

public class TextInputLayout extends android.support.design.widget.TextInputLayout{


    public TextInputLayout(Context context) {
        super(context);
    }

    public TextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public void setErrorEnabled(boolean enabled) {
        super.setErrorEnabled(true);
        if (enabled) {
            return;
        }
        if (getChildCount() > 1) {
            View view = getChildAt(1);
            if (view != null) {
                view.setVisibility(View.GONE);
            }
        }
    }
}